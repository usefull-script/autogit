[![forthebadge](http://forthebadge.com/images/badges/built-with-love.svg)](http://forthebadge.com) [![forthebadge](http://forthebadge.com/images/badges/powered-by-electricity.svg)](http://forthebadge.com)[![forthebadge](https://forthebadge.com/images/badges/60-percent-of-the-time-works-every-time.svg)](http://forthebadge.com)

# Auto Git

<div align="center">

<p>
  <img src="https://res.cloudinary.com/matthieudev/image/upload/t_media_lib_thumb/v1663921204/original_ckecok.png" alt="La Capsule"/>
</p></div>

# Description du Projet

### Pre-requis

- Création d'un compte GitLab
- Création d'un groupe GitLab

### Installation

Ligne 21 penser à modifier le chemin vers votre propre groupe

## Demarrage

Dans le terminal, utiliser la commande ./createRemoteRepository.sh

## Fabrique avec

- [Visual Studio Code](https://code.visualstudio.com/) - Editeur de code

## Versions

**Derniere version :** 1.0

## Auteurs

Listez le(s) auteur(s) du projet ici !

- **Pauline Laroche** _alias_ [@laroche.pauline](https://gitlab.com/laroche.pauline)

Lisez la liste des [contributeurs](https://gitlab.com/usefull-script/autogit) pour voir qui a aide au projet !

## License

Ce projet n'est pas sous licence
