#!/usr/bin/env bash

i=0
answer="Y"
git init
read -p "Combien de repositories souhaitez-vous créer ? " repositories

while [ $i -lt $repositories ]; do
	#modifier le README
	read -p "Nom du projet : " projet
	read -p "Nom du contributeur principal : " contributeur
	read -p "Alias GitLab du contributeur principal : " cAlias
	read -p "Technologies principales utilisées : " technos
	cat ~/devOps/python/autogit/READMETEMPLATE.md | sed "s/Titre du projet/$projet/g" | sed "s/John doe/$contributeur/g" | sed "s/johndoe/$cAlias/g" | sed "s/username/$cAlias/g" > README.md
	echo -e "\n## Technologies\n\n$technos" >> README.md
	git add README.md
	git commit -m "Project initialization"

	#création du repository
	read -p "Nommez votre repository : " repository
	git push --set-upstream git@gitlab.com:challenge1273751/$repository.git main

	#création des branches
	while [[ $answer == "Y" ]] || [[ $answer == "y" ]]; do
		read -p "Voulez vous ajouter une branche (Y/N) " answer
		if [[ $answer == "Y" ]] || [[ $answer == "y" ]]; then
			read -p "Nommez votre branche : " branch
			git branch $branch
			git checkout $branch
			git push --set-upstream git@gitlab.com:challenge1273751/$repository.git main $branch
		elif [[ $answer == "N" ]] || [[ $answer == "n" ]]; then
			echo "Création de branches terminée"
		else
			read -p "Cette commande n'est pas valide, veuillez réessayer (Y pour créer une branch, N pour arrêter) " answer
		fi
	done

	((i++))
done
